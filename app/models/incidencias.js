var mongoose = require('mongoose');
Schema = mongoose.Schema;

var IncidenciaSchema = new mongoose.Schema({
    // _id: {type: Object},
    nombre: {type: String},
    dni: {type: String},
    tipo: {type: String},
    urgencia: {type: String},
    descripcion: {type: String}
});

module.exports = mongoose.model("Incidencia", IncidenciaSchema);