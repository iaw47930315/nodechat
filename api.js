require("dotenv").config();
const port = process.env.API_PORT || 6000;
var express = require("express"),
  //bodyParser = require("body-parser"),
  api = express(),
  server = require("http").createServer(api),
  path = require("path"),
  mongoose = require('mongoose');

server.listen(port, (err, res) => {
  if (err) console.log(`ERROR: Connecting APP ${err}`);
  else console.log(`API is running on port ${port}`);
});
// Connect to mongodb://devroot:devroot@localhost:27017/?authSource=admin&readPreference=primary&appname=MongoDB%20Compass&ssl=false
mongoose.connect(
  `mongodb://${process.env.MONGO_ROOT_USER}:${process.env.MONGO_ROOT_PASSWORD}@${process.env.MONGO_URI}:${process.env.MONGO_PORT}/${process.env.MONGO_DB}?authSource=admin`,
  { useCreateIndex: true, useUnifiedTopology: true, useNewUrlParser: true },
  (err, res) => {
    if (err) console.log(`ERROR: connecting to Database.  ${err}`);
    else console.log(`Database Online: ${process.env.MONGO_DB}`);
    //console.log(`mongodb://${process.env.MONGO_ROOT_USER}:${process.env.MONGO_ROOT_PASSWORD}@${process.env.MONGO_URI}:${process.env.MONGO_PORT}/${process.env.MONGO_DB}?authSource=admin`);
  }
);
var apiRoute = require("./app/routes/api");
api.use('/api',(req, res, next) =>{
    req.isApi=true;
    next();
});
api.use("/api", apiRoute);

module.exports = api;
